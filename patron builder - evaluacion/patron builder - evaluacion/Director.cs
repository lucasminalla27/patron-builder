﻿using System;
using System.Collections.Generic;
using System.Text;

namespace patron_builder___evaluacion
{
    public class  Director
    {
        private IPocionBuilder pocionBuilder;

        public IPocionBuilder Builder
        {
            set { pocionBuilder = value; }
        }

        // El Director puede construir varias variaciones de productos usando el mismo
        // pasos de construcción.
        public void Stockminimodeingredientesparapociones()
        {
            this.pocionBuilder.ingredienteparacolor();
        }

        public void stockmaximodeingredientesparapociones()
        {
            this.pocionBuilder.ingredienteparacolor();
            this.pocionBuilder.ingredientetoxico();
            this.pocionBuilder.ingredienterelajate();
        }
    }
}
