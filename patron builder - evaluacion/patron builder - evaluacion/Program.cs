﻿using System;

namespace patron_builder___evaluacion
{
    class Program
    {
        static void Main(string[] args)
        {
            // Una tienda vende pociones por lo clasifica sus ingredientes. Crear un metodo que muestre un stock con un ingrediente y con varios y una lista personalizada 
            // El código del cliente crea un objeto constructor, lo pasa al
            // director y luego inicia el proceso de construcción.
            // Al final el resultado se recupera del objeto constructor.
            var director = new Director();
            var builder = new BuilderConcreto();
            director.Builder = builder;

            Console.WriteLine("Stock previo de ingredientes para pociones:");
            director.Stockminimodeingredientesparapociones();
            Console.WriteLine(builder.GetPociones().ListParts());

            Console.WriteLine("Stock actualizafo de ingredientes para pociones");
            director.stockmaximodeingredientesparapociones();
            Console.WriteLine(builder.GetPociones().ListParts());

            Console.WriteLine("Lista de ingredientes personlizada:");
            builder.ingredienterelajate();
            builder.ingredientetoxico();
            Console.Write(builder.GetPociones().ListParts());
        }
    }
}
