﻿using System;
using System.Collections.Generic;
using System.Text;

namespace patron_builder___evaluacion
{
   public class BuilderConcreto:IPocionBuilder
    {
        private Pociones pociones = new Pociones();
        // creacion de una instancia al contructor que contiene un objeto de pociones en blanco que se una posteriormente 

        public BuilderConcreto()
        {
            this.Reset();
        }

        public void Reset()
        {
            this.pociones = new Pociones();
        }
        // uso de la misma instancia para agregar contenido a los diferentes tipos de ingredientes
       
        public void ingredienteparacolor()
        {
            this.pociones.Add("Rosas");
        }

        public void ingredienterelajate()
        {
            this.pociones.Add("Hojas de valeriana");        }

        public void ingredientetoxico()
        {
            this.pociones.Add("Aconito");
        }
       
        // Se espera que la instancia esté lista para comenzar a producir otro producto.
        // por lo que se  llama al método de reinicio al final  cuerpo del método `GetProduct`. 
        public Pociones GetPociones()
        {
            Pociones result = this.pociones;

            this.Reset();

            return result;
        }
    }
}
