﻿using System;
using System.Collections.Generic;
using System.Text;

namespace patron_builder___evaluacion
{
    // La interfaz de Builder especifica métodos para crear las diferentes partes
    // de los objetos pociones.
    public interface IPocionBuilder
    {
        void ingredienteparacolor();

        void ingredienterelajate();

        public void ingredientetoxico();
    }
}
